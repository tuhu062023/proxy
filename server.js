const express = require('express')
const app = express()
const shell = require('shelljs')
const IP4 = shell.exec(`curl -4 -s ifconfig.co`).stdout.replace('\n', '')
const IP6 = shell.exec(`curl -6 -s ifconfig.co | cut -f1-4 -d':'`).stdout.replace('\n', '')
const NODE_PORT = 8080
shell.exec(`iptables -I INPUT -p tcp --dport ${NODE_PORT} -m state --state NEW -j ACCEPT`)
shell.exec('service network restart')
const listPort = {
  21001: null,
  21002: null,
  21003: null,
  21004: null,
  21005: null,
  21006: null,
  21007: null,
  21008: null,
  21009: null,
  21010: null,
  21011: null,
  21012: null,
}
const resetProxy3 = () => {
  const IP_AUTH = shell.cat(`IP_AUTH`).stdout.replace(/\n|\s/g, '') || ''
  const IP_AUTH2 = shell.cat(`IP_AUTH2`).stdout.replace(/\n|\s/g, '') || ''
  const authIps = [IP_AUTH, IP_AUTH2].filter(Boolean).join(',')
  const command = [
    'daemon',
    'maxconn 2000',
    'nscache 65536',
    'nscache6 65536',
    'timeouts 1 5 30 60 180 1800 15 60',
    'setgid 65535',
    'setuid 65535',
    'stacksize 262144',
    'flush',
    '',
  ]
  const proxyData = []
  for (const port in listPort) {
    if (Object.hasOwnProperty.call(listPort, port)) {
      const ipV6 = listPort[port]
      proxyData.push(`${port} => ${ipV6}`)
      command.push('auth iponly')
      command.push(`allow * ${authIps}`)
      command.push(`proxy -6 -n -a -p${port} -i${IP4} -e${ipV6}`)
      command.push(`flush`)
      command.push(``)
    }
  }
  const old3proxyProcess = shell.exec('systemctl status 3proxy').stdout?.match(/\d+(?= \/usr)/g) || []
  shell.exec(`echo "${proxyData.join('\n')}" >./_data.txt`)
  shell.exec(`echo "${command.join('\n')}" >/usr/local/etc/3proxy/3proxy.cfg`)
  shell.exec(`systemctl stop 3proxy`)
  shell.exec(`systemctl start 3proxy`)
  for (const old3proxy of old3proxyProcess) {
    try {
      shell.exec(`kill ${old3proxy}`)
    } catch (kill3ProxyProcessError) {
      console.log(kill3ProxyProcessError)
    }
  }
}
const randomIpV6 = () => {
  const str16bit = '0123456789abcdef'
  const ip64 = () => [0, 0, 0, 0].map(() => str16bit.charAt(Math.floor(Math.random() * str16bit.length))).join('')
  return `${IP6}:${ip64()}:${ip64()}:${ip64()}:${ip64()}`
}
const addIpTables = (port) => {
  shell.exec(`iptables -I INPUT -p tcp --dport ${port} -m state --state NEW -j ACCEPT`)
}

const addIfConfig = (port, ipV6) => {
  shell.exec(`ifconfig eth0 inet6 add ${ipV6}/64`)
  listPort[port] = ipV6
}

for (const port in listPort) {
  addIpTables(port)
  addIfConfig(port, randomIpV6())
}
resetProxy3()

app.listen(NODE_PORT, () => {
  console.log(`Server is running on port ${NODE_PORT}.`)
})

// app.get('/xoay', async (req, res) => {
//   const port = req.query.port
//   if (Object.hasOwnProperty.call(listPort, port)) {
//     const ipV6 = randomIpV6()
//     console.log(`Rotate ${port} => ${ipV6}`)
//     addIfConfig(port, ipV6)
//     resetProxy3()
//     return res.status(200).json({ port, ipV6, success: true })
//   }
//   return res.status(400).json({ message: `not found port: ${port}` })
// })
const rotateProxyTask = () => {
  setInterval(() => {
    for (const port in listPort) {
      addIfConfig(port, randomIpV6())
    }
    resetProxy3()
  }, 1000 * 60 * 5) // 5 minutes
}
rotateProxyTask()
const checkAndRestartServerTask = () => {
  const timeoutGitPull = 1000 * 60 * 5 // 5 minutes
  const timeoutReboot = 1000 * 60 * 60 * 5 // 5 hours
  let time = 0
  setInterval(() => {
    time += timeoutGitPull
    if (time >= timeoutReboot) {
      return shell.exec('reboot')
    }
    shell.exec('git checkout -- .')
    if (shell.exec('git pull').stdout.includes('Updating')) {
      // resetProxy3()
      return shell.exec('reboot')
    }
  }, timeoutGitPull)
}
checkAndRestartServerTask()
