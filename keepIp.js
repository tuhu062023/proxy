const shell = require('shelljs')
let curentIp
const keepIp = () => {
  let newIp = shell.exec(`curl -4 -s ifconfig.co`, { silent: true, windowsHide: true }).stdout.replace('\n', '')
  if (curentIp !== newIp) {
    curentIp = newIp
    shell.exec(`echo ${newIp}>IP_AUTH2`, { silent: true, windowsHide: true })
    shell.exec(`git add . && git commit -m UPDATE_IP_AUTH2 && git push`, { silent: true, windowsHide: true })
  }
}
keepIp();
setInterval(() => {
  console.log('run keep');
  keepIp()
}, 1000 * 60 * 5) //5 minutes
